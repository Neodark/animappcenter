package pe.haroldev.animation
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import com.microsoft.appcenter.distribute.Distribute
import com.microsoft.appcenter.distribute.UpdateTrack


class MainActivity : AppCompatActivity() {

    lateinit var imvPika: ImageView
    lateinit var imvRaic: ImageView
    var isPikachuVisible : Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Distribute.setListener(MyDistributeListener())
        Distribute.setEnabled(true);
        // not sure if this will work beyond the production release
        Distribute.setUpdateTrack(UpdateTrack.PUBLIC);

        AppCenter.start(
            application, "826c3a7b-60b5-4845-bec2-26926dc9f1b3", Distribute::class.java,
            Analytics::class.java, Crashes::class.java
        )

        Distribute.setEnabledForDebuggableBuild(false);

        setContentView(R.layout.activity_main)
        imvPika = findViewById(R.id.imv_pika)
        imvRaic = findViewById(R.id.imv_raic)
        imvPika.setOnClickListener(View.OnClickListener {
            if(isPikachuVisible){
                Analytics.trackEvent("Raichu")
                isPikachuVisible = false
                imvPika.animate().alpha(0f).duration = 2000
                imvRaic.animate().alpha(1f).duration = 2000
            }else{
                Analytics.trackEvent("Pikachu")
                isPikachuVisible = true
                imvPika.animate().alpha(1f).duration = 2000
                imvRaic.animate().alpha(0f).duration = 2000
            }
        })
    }
}